# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=gjs-mobile
pkgver=1.73.2
pkgrel=2
epoch=2
pkgdesc="Javascript Bindings for GNOME"
url="https://wiki.gnome.org/Projects/Gjs"
arch=(x86_64 aarch64)
license=(GPL)
depends=(cairo gobject-introspection-runtime js102 dconf readline
         libsysprof-capture)
makedepends=(gobject-introspection git meson dbus)
checkdepends=(xorg-server-xvfb gtk3 gtk4)
provides=(libgjs.so "gjs=$epoch:$pkgver-$pkgrel")
conflicts=(gjs)
#options=(debug)
_commit=572f8e8c5f710f0403368af688a1288d7590f19f  # tags/1.73.2^0
source=("git+https://gitlab.gnome.org/GNOME/gjs.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd gjs
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd gjs
}

build() {
  CFLAGS="${CFLAGS/-O2/-O3} -fno-semantic-interposition"
  CXXFLAGS="${CXXFLAGS/-O2/-O3} -fno-semantic-interposition"
  LDFLAGS+=" -Wl,-Bsymbolic-functions"

  arch-meson gjs build \
    -D installed_tests=false
  meson compile -C build
}

check() {
  xvfb-run -s '-nolisten local' \
    meson test -C build --print-errorlogs
}

package() {
  depends+=(libreadline.so)
  meson install -C build --destdir "$pkgdir"
}

# vim:set sw=2 et:

